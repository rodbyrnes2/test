#!/usr/bin/php
<?php

if ($argc != 2) {
	die("Usage: run_test.php <csv-file>\n");
}

if (!file_exists($argv[1])) {
	die("{$argv[1]} not found\n");
}

if (!($f = fopen($argv[1], 'r'))) {
	die("Couldn't open {$argv[1]}\n");
}

// Parse CSV file.

$paths = [];

ini_set('auto_detect_line_endings', true);

while (!feof($f)) {
	$bits = fgetcsv($f);
	if ($bits) {
		if (count($bits) != 3) {
			die("CSV file is invalid\n");
		}
		if (!$bits[0] || !$bits[1] || $bits[0] == $bits[1] || !($bits[2] > 0) ) {
			die("CSV file is invalid\n");
		}

		// Add forward path.

		if (!array_key_exists($bits[0], $paths)) {
			$paths[$bits[0]] = [];
		}
		$paths[$bits[0]][$bits[1]] = $bits[2];

		// Add backward path.

		if (!array_key_exists($bits[1], $paths)) {
			$paths[$bits[1]] = [];
		}
		$paths[$bits[1]][$bits[0]] = $bits[2];
	}
}

fclose($f);

// Read input from command line until QUIT entered.

echo '> ';

while (($line = fgets(STDIN))) {

	$line = trim($line);

	// In practice I'd check specs like I was given to see if you literally meant only QUIT should quit, or 
	// whether you wanted to accept lowercase as well. For now I'll add the case conversion, as it makes sense to.

	if (strtoupper($line) == 'QUIT') {
		break;
	}

	// Parse and validate input.

	if (!preg_match('/^(\S+)\s+(\S+)\s+(\d+)$/', $line, $matches)) {

		echo "Invalid input\n";

	} else {

		$from = $matches[1];
		$to = $matches[2];
		$max_time = $matches[3];

		if (!array_key_exists($from, $paths) || !array_key_exists($to, $paths) || $from == $to || !($max_time > 0)) {

			echo "Invalid input\n";

		} else {

			// Ordinarily I'd store $paths in an instance variable of a class and access it via $this. For this task that's
			// overkill, but I dislike using global in principle so I'm passing it as an argument to the function instead.

			$path = find_path($paths, $from, $to, $max_time);

			if (count($path) == 0) {
				echo "Path not found\n";
			} else {
				// Note that the last component of $path is the total time.
				$total_time = array_pop($path);
				echo join(' => ', $path) . " => $total_time\n";
			}
		}
	}

	echo '> ';
}


/**
 * Function to try to find a valid path shorter than or equal to $max_time.
 * If successful, an array of path segments is returned with the final item being the total time.
 * If unsuccessful, an empty array is returned.
 */

function find_path($paths, $from, $to, $max_time)
{
	// Keep track of nodes we've visited to ensure we don't get into any loops.
	$visited = [];

	// Keep track of the total time.
	$time = 0;

	return find_subpath($paths, $from, $to, $max_time, $time, $visited);
}

function find_subpath($paths, $from, $to, $max_time, $time, $visited)
{
	$visited[$from] = true;

	// Process the next segment of the path.

	foreach ($paths[$from] as $next => $next_time) {

		if (array_key_exists($next, $visited)) {
			// We've already been to this node.
			continue;
		}

		if ($time + $next_time > $max_time) {
			// This node would make the path too long so don't consider it.
			continue;
		}

		if ($next == $to) {
			// This is the node we're trying to find!
			return [$from, $to, $time + $next_time];
		}

		// Recursively process the next node in the path.

		$subpath = find_subpath($paths, $next, $to, $max_time, $time + $next_time, $visited);
		if (count($subpath)) {
			// We've found a matching subpath. Add the current path segment to the front of
			// the list and continue back up the recursion tree.
			array_unshift($subpath, $from);
			return $subpath;
		}

		// Was not able to find a suitable path from $next so continue with the next one.
	}

	$visited[$from] = false;

	// If we get to here we've been unable to find a path from our $from node.

	return [];
}

